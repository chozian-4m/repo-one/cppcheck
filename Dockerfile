ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/redhat/ubi/ubi8-minimal
ARG BASE_TAG=8.4

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

USER root 

COPY *.rpm ./

RUN rpm -i ./cppcheck-2.5-1.el7.x86_64.rpm && \
      rm -rf *.rpm

USER daemon

WORKDIR /var/tmp

HEALTHCHECK CMD VAR1=`sha256sum /bin/cppcheck` VAR2="21094769c5cb6cff3b1bd87dd41fdb1466fbd7c96ebb3af214d1793be2752023"  [ "VAR1" == "VAR2" ] && echo "Good to go" || echo "SHA256SUM Doesn't match" && exit 1

ENTRYPOINT ["/bin/cppcheck"]
